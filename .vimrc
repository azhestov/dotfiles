syntax on  
colorscheme jellybeans

set autoindent
set smarttab
set expandtab 
set softtabstop=4
set ignorecase
set background=dark
set hlsearch
set incsearch
set noswapfile
set nobackup
set nocompatible
set nu
set nowrap
set foldmethod=expr
set foldexpr=getline(v:lnum)=~'^sub'&&getline(v:lnum+1)=~'^{'?'>1':getline(v:lnum)=~'^}'?'s1':'-1'
inoremap {<CR> {<CR>}<Esc>O
inoremap SKELC<CR> #include <stdio.h><CR>#include <stdlib.h><CR><CR>main()<CR>{<CR>return(0);<CR>}<Esc>k3<Esc>O
inoremap SKELH<CR> #!bin/sh<CR><CR>exit 0<CR><Esc>k3<Esc>o
set pastetoggle=<F12>
inoremap (<Space><Space> (<Space>);<Left><Left>
if version >= 703
    set t_Co=256
    set history=64
    set undolevels=128
    set undodir=~/.vim/undodir/
    set undofile
    set undolevels=1000
    set undoreload=10000
    set sessionoptions=curdir,buffers,tabpages,folds,options
  let g:session_autoload = 'yes'
  let g:session_autosave = 'yes'
  let g:session_verbose_messages = 1
endif



set keymap=russian-jcukenwin
set helplang=ru
set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz

set fencs=utf-8,cp1251,koi8-r,cp866
set ffs=unix,mac,dos
set fileencodings=utf8,cp1251
set iskeyword=@,48-57,_,192-255
set spelllang=ru_yo,en_us
set backspace=indent,eol,start
set whichwrap=b,s
set mouse=a
set mousemodel=popup
set mousehide
set mouse=nvir
set laststatus=2
set ruler
set showmode
set showcmd
set iminsert=0
set imsearch=0
set smarttab
set expandtab
set shiftround
set autoread
nnoremap * *N
let g:indent_guides_enable_on_vim_startup = 0
let g:indent_guides_auto_colors = 0
let g:indent_guides_color_change_percent = 5
let g:indent_guides_guide_size = 1
inoremap <silent> <c-u> <Esc>u:set paste<cr>.:set nopaste<cr>gi

function! WriteSh()
let @q = "\#\!/bin/bash\n\n\n\nexit 0"
execute "0put q"
:6
:d
:2
endfunction
autocmd BufNewFile *.sh :call WriteSh()



